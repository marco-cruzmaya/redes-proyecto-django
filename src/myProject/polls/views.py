from django.http.response import HttpResponse, HttpResponseRedirect
from django.shortcuts import render
from django.views import View
from django.shortcuts import reverse


from .models import Usuario
class Hola(View):
	template = "inicio.html"
	def get(self,request):
		return render(request,self.template)
	def post(self,request):
		try:
			usuarioInsertado = Usuario(username=request.POST['usuario'],password=request.POST['password'])
		except (ValueError):
			pass
		else:
			usuarioInsertado.save()
		return HttpResponseRedirect(reverse('Respuesta'))

class Respuesta(View):
	template = "respuesta.html"
	def get(self,request):
		lista_usuarios = Usuario.objects.order_by('id')[:5]
		context = {'lista_usuarios': lista_usuarios}
		return render(request, self.template, context)