from django.urls import path

from . import views

urlpatterns = [
    path('',views.Hola.as_view(),name='Hola'),
    path('respuesta/', views.Respuesta.as_view(),name ='Respuesta'),
]